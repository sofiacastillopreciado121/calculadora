package calculadora;

import java.util.Scanner;
public class Calculadora{
	public static void main(String[]args) {
		Scanner scanner = new
		Scanner(System.in);
		
	System.out.print("Ingrese el primer numero: ");
		double num1 = scanner.nextDouble();
	System.out.print("Ingrese el segundo numero; ");
		double num2 = scanner.nextDouble();
		
	System.out.println("Selecciona una operacion;");
	System.out.println("1. Sumar");
	System.out.println("2. Restar");
	System.out.println("3. Multiplicar");
	System.out.println("4. Dividir");
	System.out.println("5. Potencia");
	System.out.println("6. Factorial");
	
	int opcion = scanner.nextInt();
	double resultado = 0;
	
	switch(opcion) {
	case 1:
		resultado = sumar(num1, num2);
		break;
	case 2:
		resultado = restar(num1, num2);
		break;
	case 3:
		resultado = multiplicar(num1, num2);
		break;
	case 4:
		resultado = dividir(num1, num2);
		break;
	case 5:
		resultado = potencia(num1, num2);
		break;
	case 6:
		resultado = factorial((int) num1);
		break;
	default:System.out.println("Opcion invalida.");
	return;
	
	}
	System.out.println("El resultado es:" +resultado);
		scanner.close();
		}
	public static double sumar(double num1, double num2) {
		return num1+num2;
	}
	public static double restar(double num1, double num2) {
		return num1-num2;
	}
	public static double multiplicar(double num1, double num2) {
		return num1*num2;
	}
	public static double dividir(double num1, double num2) {
		if(num2 !=0) {
			return num1/num2;
		}else {
			System.out.println("No se puede dividir por cero");
		}
		return 0;
	}
	public static double potencia(double num1,double num2) {
		return Math.pow(num1, num2);
	}
	public static int factorial(int n) {
		if(n<0) {
			System.out.println("El factorial no esta definido para numeros negativos.");
		}
		if (n==0 ||n==1) {
			return 1;
		}else {
			return n*factorial(n-1);
		}
	}
	}			